module XcoDate
  begin
    require 'i18n'
  rescue LoadError
    ABBR_DAY_NAMES   = %w{
      sun
      mon
      tue
      wed
      thu
      fri
      sat
    }
    ABBR_MONTH_NAMES = %w{
      ''
      jan
      feb
      mar
      apr
      may
      jun
      jul
      aug
      sep
      oct
      nov
      dec
    }
    DAY_NAMES        = %w{
      sunday
      monday
      tuesday
      wednesday
      thursday
      friday
      saturday
    }
    MONTH_NAMES      = %w{
      ''
      january
      february
      march
      april
      may
      june
      july
      august
      september
      october
      november
      december
    }
  else
    I18n.load_path << Dir[File.expand_path('config/locales') + '/*.yml']
    # I18n.default_locale = :en # (note that `en` is already the default!)

    ABBR_DAY_NAMES   = I18n.t('date.abbr_day_names')  .map(&:to_s).map(&:downcase)
    ABBR_MONTH_NAMES = I18n.t('date.abbr_month_names').map(&:to_s).map(&:downcase)
    DAY_NAMES        = I18n.t('date.day_names')       .map(&:to_s).map(&:downcase)
    MONTH_NAMES      = I18n.t('date.month_names')     .map(&:to_s).map(&:downcase)
  end

  DAY_REGEX = %r{
    #{DAY_NAMES.join('|')}
    |
    #{ABBR_DAY_NAMES.join('|')}
  }x
  MONTH_REGEX = %r{
    #{MONTH_NAMES[1..-1].join('|')}
    |
    #{ABBR_MONTH_NAMES[1..-1].join('|')}
  }x
end
