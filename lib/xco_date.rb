# frozen_string_literal: true
require 'xco_date/version'
require 'xco_date/locale'

require 'date'

module XcoDate
  class Error < StandardError; end

  class Handler
    def initialize(matcher:, start_date:, end_date:)
      @matcher = matcher
      define_singleton_method :start_date, start_date
      define_singleton_method :end_date,   end_date

      XcoDate.handlers << self
    end

    def match(s)
      s.downcase.match(@matcher)
    end

    private

    def month_index(name)
      MONTH_NAMES.find_index(name) || ABBR_MONTH_NAMES.find_index(name)
    end
  end

  class <<self
    def parse(string)
      @handlers.lazy.filter_map { |h|
        (md = h.match(string)) ? [h.start_date(md), h.end_date(md)] : nil
      }.first || [(d=Date.parse(string)), d]
    end

    def handlers
      @handlers ||= []
    end

    def clear_handlers
      @handlers.clear
    end

    def load_default_handlers
      Handler.new( # May 17-19, 2022
        matcher: /\A(#{MONTH_REGEX}) ([0-3]?[0-9])\s*-\s*([0-3]?[0-9]), (-?[0-9]{2,})\z/,
        start_date: ->(m){Date.parse("#{m[1]} #{m[2]}, #{m[4]}")},
        end_date:   ->(m){Date.parse("#{m[1]} #{m[3]}, #{m[4]}")}
      )
      Handler.new( # August 30-September 2, 2022
        matcher: /\A(#{MONTH_REGEX}) ([0-3]?[0-9])\s*-\s*(#{MONTH_REGEX}) ([0-3]?[0-9]), (-?[0-9]{2,})\z/,
        start_date: ->(m){Date.parse("#{m[1]} #{m[2]}, #{m[5]}")},
        end_date:   ->(m){Date.parse("#{m[3]} #{m[4]}, #{m[5]}")}
      )
      Handler.new( # 30 juin 2022
        matcher: /\A([0-3]?[0-9]) (#{MONTH_REGEX}) (-?[0-9]{2,})\z/,
        start_date: ->(m){Date.strptime("#{m[3]} #{month_index m[2]} #{m[1]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[3]} #{month_index m[2]} #{m[1]}", '%Y %m %d')}
      )
      Handler.new( # 17-19 mayo 2022
        matcher: /\A([0-3]?[0-9])-([0-3]?[0-9]) (#{MONTH_REGEX}) (-?[0-9]{2,})\z/,
        start_date: ->(m){Date.strptime("#{m[4]} #{month_index m[3]} #{m[1]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[4]} #{month_index m[3]} #{m[2]}", '%Y %m %d')}
      )
      Handler.new( # 30 agosto-2 septiembre 2022
        matcher: /\A([0-3]?[0-9]) (#{MONTH_REGEX})-([0-3]?[0-9]) (#{MONTH_REGEX}) (-?[0-9]{2,})\z/,
        start_date: ->(m){Date.strptime("#{m[5]} #{month_index m[2]} #{m[1]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[5]} #{month_index m[4]} #{m[3]}", '%Y %m %d')}
      )
      Handler.new( # 30 kesäkuuta 2022
        matcher: /\A([0-3]?[0-9]) (#{MONTH_REGEX})ta (-?[0-9]{2,})\z/,
        start_date: ->(m){Date.strptime("#{m[3]} #{month_index m[2]} #{m[1]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[3]} #{month_index m[2]} #{m[1]}", '%Y %m %d')}
      )
      Handler.new( # 17-19 toukokuuta 2022
        matcher: /\A([0-3]?[0-9])-([0-3]?[0-9]) (#{MONTH_REGEX})ta (-?[0-9]{2,})\z/,
        start_date: ->(m){Date.strptime("#{m[4]} #{month_index m[3]} #{m[1]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[4]} #{month_index m[3]} #{m[2]}", '%Y %m %d')}
      )
      Handler.new( # 30 elokuuta-2 syyskuuta 2022
        matcher: /\A([0-3]?[0-9]) (#{MONTH_REGEX})ta-([0-3]?[0-9]) (#{MONTH_REGEX})ta (-?[0-9]{2,})\z/,
        start_date: ->(m){Date.strptime("#{m[5]} #{month_index m[2]} #{m[1]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[5]} #{month_index m[4]} #{m[3]}", '%Y %m %d')}
      )
      Handler.new( # 2022年6月30日
        matcher: /\A(-?[0-9]{2,})年([0-1]?[0-9])月([0-3]?[0-9])日\z/,
        start_date: ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[3]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[3]}", '%Y %m %d')}
      )
      Handler.new( # 2022年5月17-19日
        matcher: /\A(-?[0-9]{2,})年([0-1]?[0-9])月([0-3]?[0-9])-([0-3]?[0-9])日\z/,
        start_date: ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[3]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[4]}", '%Y %m %d')}
      )
      Handler.new( # 2022年8月30日-9月2日
        matcher: /\A(-?[0-9]{2,})年([0-1]?[0-9])月([0-3]?[0-9])日-([0-1]?[0-9])月([0-3]?[0-9])日\z/,
        start_date: ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[3]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[1]} #{m[4]} #{m[5]}", '%Y %m %d')}
      )
      Handler.new( # 2022년 6월 30일
        matcher: /\A(-?[0-9]{2,})년 ([0-1]?[0-9])월 ([0-3]?[0-9])일\z/,
        start_date: ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[3]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[3]}", '%Y %m %d')}
      )
      Handler.new( # 2022년 5월 17-19일
        matcher: /\A(-?[0-9]{2,})년 ([0-1]?[0-9])월 ([0-3]?[0-9])-([0-3]?[0-9])일\z/,
        start_date: ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[3]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[4]}", '%Y %m %d')}
      )
      Handler.new( # 2022년 8월 30일-9월 2일
        matcher: /\A(-?[0-9]{2,})년 ([0-1]?[0-9])월 ([0-3]?[0-9])일-([0-1]?[0-9])월 ([0-3]?[0-9])일\z/,
        start_date: ->(m){Date.strptime("#{m[1]} #{m[2]} #{m[3]}", '%Y %m %d')},
        end_date:   ->(m){Date.strptime("#{m[1]} #{m[4]} #{m[5]}", '%Y %m %d')}
      )
    end

    XcoDate.load_default_handlers
  end
end
