# frozen_string_literal: true
require 'bundler/setup'
require 'xco_date'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

def reinit_locales
  I18n.load_path = Dir[File.join(Gem::Specification.find_by_name('rails-i18n').gem_dir, 'rails/locale/*.yml')] if Gem.loaded_specs.has_key?('rails-i18n')

  load 'lib/xco_date/locale.rb'
  XcoDate.clear_handlers
  XcoDate.load_default_handlers
end
