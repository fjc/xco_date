# frozen_string_literal: true

RSpec.describe XcoDate do
  it 'has a version number' do
    expect(XcoDate::VERSION).not_to be nil
  end

  it 'does something useful' do
    expect(XcoDate.parse('June 30, 2022'))                .to eq([Date.parse('June 30, 2022'),   Date.parse('June 30, 2022')    ])
  end
  it 'does something useful' do
    expect(XcoDate.parse('May 17-19, 2022'))              .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
  end
  it 'does something useful' do
    expect(XcoDate.parse('August 30-September 2, 2022'))  .to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
  end

  it 'does something useful' do
    expect(XcoDate.parse('May 17 - 19, 2022'))            .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
  end
  it 'does something useful' do
    expect(XcoDate.parse('August 30 - September 2, 2022')).to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
  end

  it 'does something useful (ko)' do
    expect(XcoDate.parse('2022년 6월 30일'))       .to eq([Date.parse('June 30, 2022'),   Date.parse('June 30, 2022')    ])
  end
  it 'does something useful (ko)' do
    expect(XcoDate.parse('2022년 5월 17-19일'))    .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
  end
  it 'does something useful (ko)' do
    expect(XcoDate.parse('2022년 8월 30일-9월 2일')).to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
  end

  it 'does something useful (ja)' do
    expect(XcoDate.parse('2022年6月30日'))       .to eq([Date.parse('June 30, 2022'),   Date.parse('June 30, 2022')    ])
  end
  it 'does something useful (ja)' do
    expect(XcoDate.parse('2022年5月17-19日'))    .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
  end
  it 'does something useful (ja)' do
    expect(XcoDate.parse('2022年8月30日-9月2日')).to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
  end

  if Gem.loaded_specs.has_key?('i18n')
    context 'locale :en' do
      before(:all) do
        I18n.locale = :en
        reinit_locales
      end

      it 'does something useful (en)' do
        expect(XcoDate.parse('June 30, 2022'))                .to eq([Date.parse('June 30, 2022'),   Date.parse('June 30, 2022')    ])
      end
      it 'does something useful (en)' do
        expect(XcoDate.parse('May 17-19, 2022'))              .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
      end
      it 'does something useful (en)' do
        expect(XcoDate.parse('August 30-September 2, 2022'))  .to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
      end

      it 'does something useful (en)' do
        expect(XcoDate.parse('May 17 - 19, 2022'))            .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
      end
      it 'does something useful (en)' do
        expect(XcoDate.parse('August 30 - September 2, 2022')).to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
      end
    end

    context 'locale :fi' do
      before(:all) do
        I18n.locale = :fi
        reinit_locales
      end

      it 'does something useful (fi)' do
        expect(XcoDate.parse('30 kesäkuuta 2022'))           .to eq([Date.parse('June 30, 2022'),   Date.parse('June 30, 2022')    ])
      end
      it 'does something useful (fi)' do
        expect(XcoDate.parse('17-19 toukokuuta 2022'))       .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
      end
      it 'does something useful (fi)' do
        expect(XcoDate.parse('30 elokuuta-2 syyskuuta 2022')).to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
      end
    end
  end

  if Gem.loaded_specs.has_key?('rails-i18n')
    context 'locale :de' do
      before(:all) do
        I18n.locale = :de
        reinit_locales
      end

      it 'does something useful (de)' do
        expect(XcoDate.parse('30 Juni 2022'))              .to eq([Date.parse('June 30, 2022'),   Date.parse('June 30, 2022')    ])
      end
      it 'does something useful (de)' do
        expect(XcoDate.parse('17-19 Mai 2022'))            .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
      end
      it 'does something useful (de)' do
        expect(XcoDate.parse('30 August-2 September 2022')).to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
      end
    end

    context 'locale :es' do
      before(:all) do
        I18n.locale = :es
        reinit_locales
      end

      it 'does something useful (es)' do
        expect(XcoDate.parse('30 junio 2022'))              .to eq([Date.parse('June 30, 2022'),   Date.parse('June 30, 2022')    ])
      end
      it 'does something useful (es)' do
        expect(XcoDate.parse('17-19 mayo 2022'))            .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
      end
      it 'does something useful (es)' do
        expect(XcoDate.parse('30 agosto-2 septiembre 2022')).to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
      end
    end

    context 'locale :fr' do
      before(:all) do
        I18n.locale = :fr
        reinit_locales
      end

      it 'does something useful (fr)' do
        expect(XcoDate.parse('30 juin 2022'))            .to eq([Date.parse('June 30, 2022'),   Date.parse('June 30, 2022')    ])
      end
      it 'does something useful (fr)' do
        expect(XcoDate.parse('17-19 mai 2022'))          .to eq([Date.parse('May 17, 2022'),    Date.parse('May 19, 2022')     ])
      end
      it 'does something useful (fr)' do
        expect(XcoDate.parse('30 août-2 septembre 2022')).to eq([Date.parse('August 30, 2022'), Date.parse('September 2, 2022')])
      end
    end
  end
end
